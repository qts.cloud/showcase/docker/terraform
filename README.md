# Prepare Terraform Docker Image

This is a base image which has Terraform, Terragrunt and Xterrafile installed.
It's purpose is to be used in the CI of other jobs.
