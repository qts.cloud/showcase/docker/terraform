ARG TERRAFORM_VERSION=0.12.19

FROM hashicorp/terraform:${TERRAFORM_VERSION}

ARG TERRAGRUNT_VERSION=v0.21.11
ARG TERRAFILE_VERSION=2.3.1

ENV TERRAFORM_VERSION=${TERRAFORM_VERSION}
ENV TERRAGRUNT_VERSION=${TERRAGRUNT_VERSION}
ENV TERRAFILE_VERSION=${TERRAFILE_VERSION}

RUN apk add --no-cache git curl jq
RUN curl -L https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 -o /usr/local/bin/terragrunt && chmod +x /usr/local/bin/terragrunt
RUN curl -L https://github.com/devopsmakers/xterrafile/releases/download/v${TERRAFILE_VERSION}/xterrafile_${TERRAFILE_VERSION}_Linux_x86_64.tar.gz | tar xz -C /usr/local/bin

ENTRYPOINT [ "/usr/bin/env" ]